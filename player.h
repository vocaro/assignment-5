#pragma once

#include <string>

// Stores a player's name, points scored, etc.
class FootballPlayer {
private:
    std::string _name;
    int _points;
public:
    FootballPlayer(const std::string &name);
    const std::string &getName() const;
    int getPointsScored() const;
    void recordScore(int points);
};
