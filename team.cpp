#include "team.h"

#include "player.h"

#include <string>

// Maximum allowed number of players.
const int MAX_PLAYERS = 100;

using namespace std;

// Constructs a new football team having the given name.
FootballTeam::FootballTeam(const string &name)
{
    _name = name;
    _players = new FootballPlayer*[MAX_PLAYERS];
    _numPlayers = 0;
}

FootballTeam::~FootballTeam()
{
    for (int i = 0; i < _numPlayers; i++) {
        delete _players[i];
    }

    delete [] _players;
}

// Adds a new football player with the given name to the roster. Fails if the maximum
// has been reached.
void FootballTeam::addPlayer(const string &name)
{
    if (_numPlayers == MAX_PLAYERS) {
        return;
    }
    
    _players[_numPlayers++] = new FootballPlayer(name);
}

// Returns a FootballPlayer pointer to the player with the given name, or nullptr if not found.
FootballPlayer *FootballTeam::getPlayer(const string &name) const
{
    for (int i = 0; i < _numPlayers; i++) {
        if (_players[i]->getName() == name) {
            return _players[i];
        }
    }
    
    return nullptr;
}

// Returns the highest scoring player on the team, or nullptr if there are no players.
// If two or more players have the same score, the first player added will be returned.
FootballPlayer *FootballTeam::getHighestScorer() const
{
    if (_numPlayers == 0) {
        return nullptr;
    }
    
    FootballPlayer *highestScorer = _players[0];
    
    for (int i = 1; i < _numPlayers; i++) {
        if (_players[i]->getPointsScored() > highestScorer->getPointsScored()) {
            highestScorer = _players[i];
        }
    }
    
    return highestScorer;
}

// Returns the total points scored among all players on the team.
int FootballTeam::getScore() const {
    int score = 0;
    for (int i = 0; i < _numPlayers; i++) {
        score += _players[i]->getPointsScored();
    }
    return score;
}
