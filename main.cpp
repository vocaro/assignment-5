#include "parser.h"

#include <iostream>
#include <string>

using namespace std;

int main(int argc, char **argv)
{
    if (argc != 2) {
        cout << "Usage: football FILENAME" << endl;
        return 1;
    }

    string filename = argv[1];
    FootballFileParser parser(filename);

    if (!parser.read()) {
        cerr << "Could not read from " << filename << endl;
    }

    parser.printStats();

    return 0;
}
