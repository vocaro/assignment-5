#include "parser.h"

#include "game.h"

#include <iostream>
#include <sstream>
#include <string>

using namespace std;

// Initializes the file parser.
FootballFileParser::FootballFileParser(const string &filepath)
{
    _filepath = filepath;
    game = nullptr;
}

FootballFileParser::~FootballFileParser()
{
    if (game != nullptr) {
        delete game;
    }
}

// Reads the football data file into memory. Returns true on success; false otherwise.
bool FootballFileParser::read()
{
    ifstream inputFile(_filepath);
    if (!inputFile) {
        return false;
    }
    
    string homeName, visitorsName;
    string homePlayers, visitorsPlayers;
    if (!getline(inputFile, homeName) ||
        !getline(inputFile, homePlayers) ||
        !getline(inputFile, visitorsName) ||
        !getline(inputFile, visitorsPlayers)) {
        return false;
    }
    
    game = new FootballGame(homeName, visitorsName);

    parsePlayerNames(HOME, homePlayers);
    parsePlayerNames(VISITORS, visitorsPlayers);
    parseScores(inputFile);
    
    return true;
}

// Helper function that takes a string of comma-separated player names
// and adds them to the game.
void FootballFileParser::parsePlayerNames(Side side, const string &line)
{
    stringstream tokenizer(line);
    string name;
    char token = ',';

    while (getline(tokenizer, name, token)) {
        game->addPlayer(side, ltrim(name));  // Left-trim the input to remove spaces
    }
}

// Helper function to read lines from the file containing football score data
// and record them in the FootballGame object. Stops when the end of the file
// is reached or an error occurs.
void FootballFileParser::parseScores(ifstream &inputFile)
{
    string line;

    while (getline(inputFile, line)) {
        stringstream tokenizer(line);
        char token = ',';
        string sideStr, player, pointsStr;
        
        if (getline(tokenizer, sideStr, token) &&
            getline(tokenizer, player, token) && 
            getline(tokenizer, pointsStr, token)) {
            // ltrim is used to left-trim the input to remove spaces
            game->recordScore(getSideFromString(ltrim(sideStr)),
                              ltrim(player),
                              getPointsFromString(ltrim(pointsStr)));
        }
    }
}

// Converts a string code ("H" or "V") to a Side enum value. Aborts the program
// if an invalid string is encountered.
Side FootballFileParser::getSideFromString(const string &sideStr) const
{
    if (sideStr == "H") {
        return HOME;
    } else if (sideStr == "V") {
        return VISITORS;
    } else {
        cerr << "Unexpected side code: " << sideStr << endl;
        exit(1);
    }
}

// Converts a string code ("TD", "FG", "S", or "EP") to a point value. Aborts the
// program if an invalid string is encountered.
int FootballFileParser::getPointsFromString(const string &pointsStr) const
{
    if (pointsStr == "TD") {
        return 6;
    } else if (pointsStr == "FG") {
        return 3;
    } else if (pointsStr == "S") {
        return 2;
    } else if (pointsStr == "EP") {
        return 1;
    } else {
        cerr << "Unexpected point code: " << pointsStr << endl;
        exit(1);
    }
}

// Helper function to remove spaces from the left side of the given string.
// Returns a new string with the spaces removed.
string FootballFileParser::ltrim(const string &str) const
{
    string retval = "";

    int i = 0;
    while (str[i] == ' ' && i < str.length()) {
        i++;
    }

    while (i < str.length()) {
        retval += str[i];
        i++;
    }
    
    return retval;
}

string FootballFileParser::mvpAsString(const FootballTeamStats &teamStats) const
{
    return teamStats.mvpName + " (" + to_string(teamStats.mvpScore) + (teamStats.mvpScore == 1 ? " point" : " points") + ")";
}

// Prints a summary of the game, including point totals, winner, and highest scorers on each team.
void FootballFileParser::printStats() const
{
    FootballGameStats stats = game->getStats();
    
    cout << "Home: " << stats.homeStats.score << endl;
    cout << "Visitors: " << stats.visitorsStats.score << endl;

    // This is a special case to handle a bug in FootballGameStats, as it can't express a tie game.
    // Probably would have been better if FootballGameStats did not have a "winner" field at all.
    if (stats.homeStats.score == stats.visitorsStats.score) {
        cout << "Winner: none" << endl;
    } else {
        switch (stats.winner) {
            case HOME:
            cout << "Winner: Home" << endl;
            break;
            case VISITORS:
            cout << "Winner: Visitors" << endl;
            break;
        }
    }
    
    cout << "Highest scorers:" << endl;
    cout << "  Home: " << mvpAsString(stats.homeStats) << endl;
    cout << "  Visitors: " << mvpAsString(stats.visitorsStats) << endl;
}
