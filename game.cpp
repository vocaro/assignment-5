#include "game.h"

#include "player.h"
#include "team.h"

#include <iostream>
#include <string>

using namespace std;

// Constructs a new football game object with the given team names.
FootballGame::FootballGame(const string &homeName, const string &visitorsName)
{
    _home = new FootballTeam(homeName);
    _visitors = new FootballTeam(visitorsName);
}

FootballGame::~FootballGame()
{
    delete _home;
    delete _visitors;
}

// Adds a new player to the given side having the given name.
void FootballGame::addPlayer(Side side, const string &name)
{
    switch (side) {
        case HOME:
        _home->addPlayer(name);
        break;
        case VISITORS:
        _visitors->addPlayer(name);
        break;
    }
}

// Adds the given number of points to the named player. Aborts the program with
// an error message if the player could not be found.
void FootballGame::recordScore(Side side, const string& playerName, int points)
{
    FootballPlayer *player;
    
    switch (side) {
        case HOME:
        player = _home->getPlayer(playerName);
        break;
        case VISITORS:
        player = _visitors->getPlayer(playerName);
        break;
    }
    
    if (player != nullptr) {
        player->recordScore(points);
    } else {
        cerr << "Could not record score for missing player: " << playerName << endl;
        exit(1);
    }
}

FootballGameStats FootballGame::getStats() const
{
    FootballGameStats stats;

    fillTeamStats(_home, stats.homeStats);
    fillTeamStats(_visitors, stats.visitorsStats);

    // Note: This doesn't handle tie games. There's no way to express that with the "winner" field as-is.
    stats.winner = stats.homeStats.score > stats.visitorsStats.score ? HOME : VISITORS;
    
    return stats;
}

void FootballGame::fillTeamStats(FootballTeam *team, FootballTeamStats &teamStats) const
{
    teamStats.score = team->getScore();

    FootballPlayer *mvp = team->getHighestScorer();

    if (mvp == nullptr) {
        teamStats.mvpName = "none";
        teamStats.mvpScore = 0;
    } else {
        teamStats.mvpName = mvp->getName();
        teamStats.mvpScore = mvp->getPointsScored();
    }
}
