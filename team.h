#pragma once

#include "player.h"

#include <string>

// Manages a football team, including name and players.
// Can compute the highest point scorer on the team.
class FootballTeam {
private:
    std::string _name;
    FootballPlayer **_players;
    int _numPlayers;
public:
    FootballTeam(const std::string &name);
    ~FootballTeam();
    void addPlayer(const std::string &name);
    FootballPlayer *getPlayer(const std::string &name) const;
    int getScore() const;
    FootballPlayer *getHighestScorer() const;
};
