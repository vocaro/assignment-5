#pragma once

#include "football.h"
#include "game.h"

#include <fstream>
#include <string>

// Parses a file containing CSV data on a football game in the format described here:
// https://gitlab.com/vocaro/assignment-5/-/blob/main/README.md
// Can also print a summary of the game.
class FootballFileParser {
private:
    std::string _filepath;
    FootballGame *game; 
    void parsePlayerNames(Side side, const std::string &line);
    void parseScores(std::ifstream &inputFile);
    Side getSideFromString(const std::string &sideStr) const;
    int getPointsFromString(const std::string &pointsStr) const;
    std::string ltrim(const std::string &str) const;
    std::string mvpAsString(const FootballTeamStats &teamStats) const;
public:
    FootballFileParser(const std::string &filepath);
    ~FootballFileParser();
    bool read();  // Returns false if parsing fails
    void printStats() const;
};
