#include "player.h"

#include <string>

using namespace std;

// Initializes the football player with a name.
FootballPlayer::FootballPlayer(const string &name)
{
    _name = name;
}

// Returns the football player's name.
const string &FootballPlayer::getName() const
{
    return _name;
}

// Returns the football player's total points scored.
int FootballPlayer::getPointsScored() const
{
    return _points;
}

// Adds the given points to the player's total. Must be a valid football score.
void FootballPlayer::recordScore(int points)
{
    if (points != 1 && points != 2 && points != 3 && points != 6) {
        return;
    }

    _points += points;
}
