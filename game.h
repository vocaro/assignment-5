#pragma once

#include "football.h"
#include "team.h"

#include <string>

struct FootballTeamStats {
    int score;
    std::string mvpName;
    int mvpScore;
};

struct FootballGameStats {
    Side winner;
    FootballTeamStats homeStats;
    FootballTeamStats visitorsStats;
};

// Represents a football game played between a home and visitors team.
class FootballGame {
private:
    FootballTeam *_home;
    FootballTeam *_visitors;
    void fillTeamStats(FootballTeam *team, FootballTeamStats &teamStats) const;
public:
    FootballGame(const std::string &homeName, const std::string &visitorsName);
    ~FootballGame();
    void addPlayer(Side side, const std::string &name);
    void recordScore(Side side, const std::string& playerName, int points);
    FootballGameStats getStats() const;
};
